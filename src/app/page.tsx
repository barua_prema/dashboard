"use client";
import Head from 'next/head';
import Sidebar from './components/sidebar';
import Header from './components/header';
import Container from "../app/components/dashboard/container";

export default function Home() {
  return (

    <div className="flex w-screen h-screen" >
      <Sidebar />
      <main className="w-screen bg-gray-300">
        <div>
        <Header />
        <Container />
        
        </div>
        
      </main>
    </div>

  )
}
