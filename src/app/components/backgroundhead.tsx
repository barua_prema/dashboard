import React from 'react';
import Card from './Dashboardcard';
import Image from "next/image";
import bg from "../../../img/front-page-bg.png";

const Backgroundhead = () => {
    return (
        <>
            <div className="flex flex-row">

                {/* Backgroundhead start */}
                <div className="flex flex-row basis-4/5 bg-white rounded-lg shadow-lg p-3 m-4 w-20 h-44">
                    <div className="">
                        <h2 className="text-xl font-bold mb-2">Card Title 1</h2>
                        <p className="text-gray-600 text-sm">Lorem ipsum dolor sit amet, consectetur adipiscing elits, consectetur
                            adipiscing elit.</p>
                        <a
                            href="#"
                            className="text-blue-500 text-sm font-semibold hover:text-blue-700">
                            Read more
                        </a>
                    </div>
                    <div className="">
                        <Image
                            src={bg} className='w-40 h-auto m-2'
                            alt="bg">
                        </Image>
                    </div>
                </div>
                {/* Backgroundhead end */}

                <Card />
            </div>
        </>
    )
}

export default Backgroundhead;