import Image from "next/image";
import { FaUser, FaLock, FaFacebookF, FaLinkedinIn, FaTwitter, FaGoogle } from "react-icons/fa";
import img from "../../../img/registration.png";

export default function Registration() {
  return (
    <>
      <div className='bg-gradient-to-r from-green-300 via-yellow-50 to-green-300 block h-screen items-center justify-center p-1 md:flex'>
        {/* registration card start */}
        <div className=" bg-cover bg-image flex flex-col items-center max-w-screen-lg overflow-hidden rounded-lg shadow-lg 
        text-gray-950 w-full md:flex-row font-sans">

          <Image
            src={img}
            alt="registration" width="550" height="400">
          </Image>

          {/* Form start */}
          <div className="h-full from-green-300 flex flex-col items-center p-4 w-full md:w-1/2">
            
            <div className="flex flex-col items-center space-y-2  ">
              <h1 className="font-medium text-green-400 text-3xl pb-4"> Create Account </h1>
            </div>

            {/* Inputs */}
            <form className="flex flex-col items-center space-y-3 pt-1">
              <div className="relative pb-3">
                <span className="absolute flex inset-y-0 items-center pl-3 pb-3 text-gray-400">
                  <FaUser />
                </span>
                <input className="border boder-gray-300 outline-none placeholder-gray-400 pl-16 px-2 py-2 
                rounded-md transition focus:ring-2 focus:ring-green-400" placeholder="Full Name....." type="text"
                />
              </div>
              <div className="relative pb-3">
                <span className="absolute flex inset-y-0 items-center pl-3 pb-3 text-gray-400">
                  <FaUser />
                </span>
                <input className="border boder-gray-300 outline-none placeholder-gray-400 pl-16 px-2 py-2 
                rounded-md transition focus:ring-2 focus:ring-green-400" placeholder="Email....." type="text"
                />
              </div>
              <div className="relative pb-3">
                <span className="absolute flex inset-y-0 items-center pl-3 pb-3 text-gray-400">
                  <FaLock />
                </span>
                <input className="border boder-gray-300 outline-none placeholder-gray-400 pl-16 pr-3 py-2
                rounded-md transition focus:ring-2 focus:ring-green-400" placeholder="Password....." type="password"
                />
              </div>
              <div className="relative pb-3">
                <span className="absolute flex inset-y-0 items-center pl-3 pb-3 text-gray-400">
                  <FaLock />
                </span>
                <input className="border boder-gray-300 outline-none placeholder-gray-400 pl-16 pr-3 py-2 
                rounded-md transition focus:ring-2 focus:ring-green-400" placeholder="Confirm Password....." type="password"
                />
              </div>
              <div className="w-64 mb-2 pt-1">
                <label className="text-xs accent-green-500"><input className="mr-1" type="checkbox" name="remember" />
                  I accept the <a href="#" className="text-xs text-green-500">Terms of Use</a> & <a href="#" className="text-xs text-green-500">Privacy Policy.</a>
                </label>
              </div>
              <button className="bg-green-400 font-medium inline-flex items-center px-8 py-2 rounded-md
               text-white transition hover:bg-green-500" type="submit">
                Signup
              </button>
              <div className="w-64 mb-2 pt-1">
                <p className="text-xs accent-green-500">
                  Already have an account? <a href="#" className="text-xs text-green-500">Login</a>
                </p>
              </div>
              

              {/* Links */}
              <div className="flex justify-center my-2 pt-2">
                <a href="#" className="border-2 border-gray-950 rounded-full p-3 mx-1 hover:bg-green-500">
                  <FaFacebookF className="text-sm fill-gray-950" />
                </a>
                <a href="#" className="border-2 border-gray-950 rounded-full p-3 mx-1 hover:bg-green-500">
                  <FaLinkedinIn className="text-sm fill-gray-950" />
                </a>
                <a href="#" className="border-2 border-gray-950 rounded-full p-3 mx-1 hover:bg-green-500">
                  <FaTwitter className="text-sm fill-gray-950" />
                </a>
                <a href="#" className="border-2 border-gray-950 rounded-full p-3 mx-1 hover:bg-green-500">
                  <FaGoogle className="text-sm fill-gray-950" />
                </a>
              </div>
            </form>

          </div>
          {/* From end */}
        </div>
        {/* registration card end */}

      </div>
    </>
  )
}
